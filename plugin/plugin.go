package plugin

import (
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"
)

// PhpExtensions is a list of valid php file extensions
var PhpExtensions = []string{
	"php",
	"php3",
	"php4",
	"php5",
	"php7",
	"phtml",

	"phar", // PHAR library
	"phpt", // PHP Test file
	"phps", // PHP source
}

// Match checks if the filename has a valid php extension
func Match(path string, info os.FileInfo) (bool, error) {
	ext := strings.TrimPrefix(filepath.Ext(info.Name()), ".")
	if ext == "" {
		return false, nil
	}
	for _, phpExt := range PhpExtensions {
		if ext == phpExt {
			return true, nil
		}
	}
	return false, nil
}

func init() {
	plugin.Register("phpcs-security-audit", Match)
}
